package cz.sspbrno.ustrednitopeni.sensors;

import cz.sspbrno.ustrednitopeni.CustomMQTTConnection;
import org.eclipse.paho.client.mqttv3.*;

import java.util.UUID;

public class HumiditySensor implements MqttCallbackExtended {
    private static final String CONNECTION = "node/co2-monitor:0/hygrometer/0:4/relative-humidity";
    private IMqttClient subscriber;
    private String message = "";

    public HumiditySensor(MqttConnectOptions options) throws MqttException {
        String subscriberId = UUID.randomUUID().toString();
        subscriber = new MqttClient(CustomMQTTConnection.IP_ADDRESS, subscriberId);
        subscriber.connect(options);
        subscriber.setCallback(this);
    }

    public void run() throws MqttException {
        subscriber.subscribe(CONNECTION);
    }

    @Override
    public void connectComplete(boolean b, String s) {

    }

    @Override
    public void connectionLost(Throwable throwable) {

    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) {
        message = mqttMessage.toString();
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    public String getMessage() {
        return message;
    }

    public void clearMessage() {
        message = "";
    }
}
