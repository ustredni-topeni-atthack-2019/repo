package cz.sspbrno.ustrednitopeni;

public enum Type {
    POSX("x"),
    POSY("y"),
    TEMPERATURE("Temperature"),
    HUMIDITY("Humidity"),
    LIGHT("Light"),
    OXYGEN("Oxygen");

    public final String name;

    Type(String name){
        this.name = name;
    }
}
