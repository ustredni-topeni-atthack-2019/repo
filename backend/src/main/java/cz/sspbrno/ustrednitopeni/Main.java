package cz.sspbrno.ustrednitopeni;

public class Main {

    public static void main(String[] args) {
        try {
            String customName = null;
            if(args.length > 0)
                customName = args[0];
            new CustomMQTTConnection(customName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
