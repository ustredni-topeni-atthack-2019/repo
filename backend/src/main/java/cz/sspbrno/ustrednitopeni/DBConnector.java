package cz.sspbrno.ustrednitopeni;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;

public class DBConnector {

    private Connection connection;

    public DBConnector(String customName) {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:"+customName+".db");
            if (connection != null) {
                DatabaseMetaData meta = connection.getMetaData();
            }
            Statement stm = connection.createStatement();
            stm.setQueryTimeout(30);

            stm.executeUpdate("drop table if exists senzor");
            stm.executeUpdate("drop table if exists records");
            stm.executeUpdate("CREATE TABLE IF NOT EXISTS \"records\" (\n" +
                    "\t\"ID_records\"\tINTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "\t\"ID_senzor\"\tINTEGER NOT NULL,\n" +
                    "\t\"type\"\tTEXT NOT NULL,\n" +
                    "\t\"data\"\tTEXT NOT NULL,\n" +
                    "\t\"time\"\tINTEGER NOT NULL\n" +
                    ");");
            stm.executeUpdate("CREATE TABLE IF NOT EXISTS \"senzor\" (\n" +
                    "\t\"ID\"\tINTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "\t\"x\"\tINTEGER,\n" +
                    "\t\"y\"\tINTEGER\n" +
                    ");");

        } catch (SQLException | ClassNotFoundException e) {
            System.err.println("Can't create DB");
            e.printStackTrace();
        }
    }

    public void saveData(HashMap<Type, String> data, String senzorID) {
        try {
            Statement stm = connection.createStatement();
            StringBuilder insertData = new StringBuilder();
            for (Type key : data.keySet()) {
                if (key.equals(Type.POSX) || key.equals(Type.POSY))
                    continue;
                insertData.append(key.name);
                insertData.append("', '").append(data.get(key));
            }

            Statement result = connection.createStatement();

            ResultSet rs = result.executeQuery("SELECT ID from senzor");
            ArrayList<Integer> ids = new ArrayList<>();
            while (rs.next()) {
                ids.add(rs.getInt("ID"));
            }

            if (!ids.contains(senzorID)) {
                if (data.containsKey(Type.POSX) && data.containsKey(Type.POSY))
                    stm.executeUpdate("INSERT INTO senzor(x, y) VALUES( " + data.get(Type.POSX) + ", " + data.get(Type.POSY) + ");");
                else
                    stm.executeUpdate("INSERT INTO senzor(x, y) VALUES (1, 1 );");
            }

            LocalDateTime localDateTime = LocalDateTime.now();
            stm.executeUpdate("INSERT INTO records (type, data, time, ID_senzor) values ( '" + insertData.toString() + "', " + localDateTime.atZone(ZoneId.systemDefault()).toEpochSecond() + ", " + senzorID + ");");
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
