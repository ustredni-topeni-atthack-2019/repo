package cz.sspbrno.ustrednitopeni;

import org.json.simple.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public final class JSONSaver {

    public static void saveDataToJson(HashMap<String, String> data, String fileLocation) {
        JSONObject jsonObject = new JSONObject();
        JSONObject position = new JSONObject();
        position.put("x", data.get("x"));
        position.put("y", data.get("y"));
        jsonObject.put("position", position);
        for (Object key : jsonObject.keySet()) {
           if (key == "x" || key == "y")
               continue;
           jsonObject.put(key, data.get(key));
        }

        saveJsonData(jsonObject.toJSONString(), fileLocation);
    }

    private static void saveJsonData(String JSON, String fileLocation) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileLocation)));
        } catch (IOException e) {
            System.out.println("\b \b");
        }
    }
}
