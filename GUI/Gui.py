from tkinter import *
from GUI.database import database
from fpdf import *
import statistics
from PyPDF2 import PdfFileReader, PdfFileWriter
import time
import tkinter.constants, tkinter.filedialog
import tkdocviewer
import os

class Window:

    def __init__(self):
        self.win = Tk()
        self.win.title("OfficeCheck Reporter")
        self.height = self.win.winfo_screenheight()
        self.width = self.win.winfo_screenwidth()
        self.win.geometry(str(self.width-200)+"x" +
                          str(self.height-200)+"+100+100")

        self.database = database()
        self.pdf = FPDF()
        self.pdfEmpty = FPDF()
        self.pdfEmpty.output("tmp.pdf")

        self.type  = "Oxygen"
        self.style = "Text"
        self.var = IntVar()
        self.frams = [Frame(), Frame()]
        self.data = {"Oxygen": [], "Humidity": [],
                     "Light": [], "Temperature": [], "Senzor": []}

        self.graf_canvas = Canvas(self.frams[0], height=self.height, width=self.width)
        self.default()
        self.win.mainloop()
        

    def openFile(self):
        self.data = {"Oxygen": [], "Humidity": [],
                     "Light": [], "Temperature": [], "Senzor": []}
        tables_names = self.database.select(
            "sqlite_master", "name", condition="name!=\'sqlite_sequence\'")
        tables_data = []
        for table in self.database.select("sqlite_master", "name", condition="name!=\'sqlite_sequence\'"):
            tables_data.append(self.database.select(table[0], "*"))
        for i in range(len(tables_names)):
            if tables_names[i][0] == "records":
                for j in range(len(tables_data[i])):
                    self.data[tables_data[i][j][2]].append((tables_data[i][j][3], tables_data[i][j][4], tables_data[i][j][1]))
            elif tables_names[i][0] == "senzor":
                for j in range(len(tables_data[i])):
                    self.data["Senzor"].append((tables_data[i][j][1], tables_data[i][j][2], tables_data[i][j][0]))
        for i in self.data:
            pass
        
        self.default()

    def exportToPDF(self):
        if len(self.data["Senzor"]) > 0:
            saveLocation = tkinter.filedialog.asksaveasfilename()
            saveLocation = "results.pdf"
            self.pdf.output(saveLocation)
            os.system("gio open results.pdf")

    def showData(self):
        #WIP grafical represenatations
        """
        rad1 = Radiobutton(self.win, variable=self.var, text="Graf", value = 1, indicatoron=0, width = 13)
        rad1.place(x=0,y=0)
        rad2 = Radiobutton(self.win, variable=self.var, text="Text", value = 2, indicatoron=0, width = 13)
        rad2.place(x=100,y=0)
        rad3 = Radiobutton(self.win, variable=self.var, text="Map", value = 3, indicatoron=0, width = 13)
        rad3.place(x=200,y=0)
        for some in self.frams:
            try:
                some.place_forget()
            except:
                pass
        self.graf_canvas.pack_forget()
        self.graf_canvas=Canvas(self.frams[0], height=self.height, width=self.width)
        if self.var.get() == 1:
            self.frams[0].place(x=0,y=100)
            self.graf_canvas.pack(fill=BOTH)
            vals = buble(self.data[self.type], 1)
            for i in range(len(vals)):
                self.graf_canvas.create_line(0+i*self.width//20,0,0+i*self.width//20,self.height*5)
        """
        pdfView = tkdocviewer.DocViewer(self.win)

        state=0
        if self.type == "Oxygen":
            state=4
        elif self.type == "Humidity":
            state=5
        elif self.type == "Light":
            state=6
        elif self.type == "Temperature":
            state=7

        pdfView.place(x=0,y=0)
        pdfView.display_file("tmp.pdf", state)
        pdfView.fit_page(8, 8)

        pdfViewSidePannel = tkdocviewer.DocViewer(self.win)
        pdfViewSidePannel.place(x=720,y=0)
        pdfViewSidePannel.display_file("tmp.pdf", 3)
        pdfViewSidePannel.fit_page(8.27, 11)

            

    def oxygen(self):
        self.type = "Oxygen"
        self.showData()

    def humidity(self):
        self.type = "Humidity"
        self.showData()

    def light(self):
        self.type = "Light"
        self.showData()

    def temperature(self):
        self.type = "Temperature"
        self.showData()

    def default(self):

        # MAIN MENU
        Mainmenu = Menu(self.win, tearoff=0)

        # FILE MENU
        filemenu = Menu(Mainmenu, tearoff=0)

        filemenu.add_command(label="Open", command=self.openFile, font=("Consolas", 18))
        filemenu.add_command(label="Open recent", command=self.openFile, font=("Consolas", 18))
        filemenu.add_separator()
        filemenu.add_command(label="Export to PDF", command=self.exportToPDF, font=("Consolas", 18))

        Mainmenu.add_cascade(label="File", menu=filemenu, font=("Consolas", 18))

        # OXYGEN
        Mainmenu.add_command(label="oxygen", command=self.oxygen, font=("Consolas", 18))

        # HUMIDITY
        Mainmenu.add_command(label="humidity", command=self.humidity, font=("Consolas", 18))

        # LIGHT
        Mainmenu.add_command(label="light", command=self.light, font=("Consolas", 18))

        # TEMPERATURE
        Mainmenu.add_command(label="temperature", command=self.temperature, font=("Consolas", 18))

        # SOUPRICE
        Mainmenu.add_command(label="Exit", command=self.win.destroy, font=("Consolas", 18))

        self.win.config(menu=Mainmenu)

        if len(self.data["Senzor"]) > 0:
            self.createPDF()
        
        # SHOW
        self.showData()


    def createPDF(self):
        #CREATING CONTAINERS FOR PDF

        types = ["Oxygen", "Humidity", "Light", "Temperature"]
        typesUnits = {"Oxygen": "ppm", "Humidity": "%", "Light": "lx", "Temperature": "°C"}
        myMin = {"Oxygen": self.data["Oxygen"][0], "Humidity": self.data["Humidity"][0], "Light": self.data["Light"][0], "Temperature": self.data["Temperature"][0]}
        myMax = {"Oxygen": self.data["Oxygen"][0], "Humidity": self.data["Humidity"][0], "Light": self.data["Light"][0], "Temperature": self.data["Temperature"][0]}
        myAvg = {}

        for myType in types:
            avg = []
            for t in range(len(self.data[myType])):
                avg.append(float(self.data[myType][t][0]))
                if self.data[myType][t][0] < myMin[myType][0]:
                    myMin[myType] = self.data[myType][t]
                elif self.data[myType][t][0] > myMax[myType][0]:
                    myMax[myType] = self.data[myType][t]
            myAvg[myType] = statistics.mean(avg)

        #FIRST PAGE
        self.pdf.add_page()
        
        self.pdf.set_font("Arial", size=8)
        self.pdf.ln(5)
        self.pdf.cell(190, 5, txt="OfficeCheck", align="R", ln=1)
        self.pdf.ln(60)

        self.pdf.set_font("Arial", style="B", size=16)
        self.pdf.cell(190, 20, txt="Office Enviromental Report", align="C")
        self.pdf.ln(80)

        self.pdf.set_font("Arial", style="B", size=11)
        self.pdf.cell(190, 10, txt="Brno Exhibition Centre, pavilon E", align="C")

        self.pdf.set_font("Arial", style="I", size=9)
        self.pdf.ln(90)
        self.pdf.cell(190, 10, txt="Dan Pavlik", align="C", ln=1)
        self.pdf.cell(190, 10, txt="23. 11. 2019", align="C")


        #SECOND PAGE
        self.pdf.add_page()
        self.pdf.set_font("Arial", style="B", size=13)

        self.pdf.cell(40, 10, txt="REPORT", align="L")

        self.pdf.set_line_width(1)
        self.pdf.line(10, 32, 200, 32)
        self.pdf.ln(25)
        self.pdf.set_font("Arial", size=11)
        self.pdf.cell(200, 15, txt="Humidity", align="L", ln=1)
        self.pdf.set_font("Arial", size=10)
        if float(myMin["Humidity"][0]) <= 30.0 and float(myMax["Humidity"][0]) >= 70.0:
            self.pdf.cell(40, 10, txt="In the room/s, where senzors "+str(myMin["Humidity"][0])+" and"+str(myMax["Humidity"][0])+" was placed, we detected both low and high humidity.", align="L", ln=1)
        
        elif float(myMin["Humidity"][0]) <= 30.0:
            self.pdf.cell(40, 10, txt="In the room, where senzor "+str(myMin["Humidity"][0])+" was placed, we detected very low humidity.", align="L", ln=1)
            self.pdf.cell(40, 10, txt="This state is below regulatory levels, and you can be fined for it.", align="L", ln=1)
            self.pdf.cell(40, 10, txt="To fix this, you can open windows more often or get an air ventilation system set up.", align="L", ln=1)
        elif float(myMax["Humidity"][0]) >= 70.0:
            self.pdf.cell(40, 10, txt="In the room, where senzor "+str(myMax["Humidity"][0])+" was placed, we detected very high humidity.", align="L", ln=1)
            self.pdf.cell(40, 10, txt="This state is over the regulatory levels, and you can be fined for it.", align="L", ln=1)
            self.pdf.cell(40, 10, txt="To fix this, you can install a dehumidifier.", align="L", ln=1)
        else:
            self.pdf.cell(40, 10, txt="In the room/s, where senzors was placed, we detected normal humidity levels", align="L", ln=1)

        self.pdf.ln(20)
        self.pdf.set_font("Arial", size=11)
        self.pdf.cell(200, 15, txt="Light", align="L", ln=1)
        self.pdf.set_font("Arial", size=10)


        if float(myMin["Light"][0]) <= 300.0:
            self.pdf.cell(40, 10, txt="In the room, where sensor "+str(myMin["Light"][0])+" was placed, we detected low amount of light.", align="L", ln=1)
            self.pdf.cell(40, 10, txt="To fix this, we recommend installing better lights into this area.", align="L", ln=1)
        else:
            self.pdf.cell(40, 10, txt="In the room/s, where senzors was placed, we detected normal light conditions", align="L", ln=1)

        self.pdf.ln(20)
        self.pdf.set_font("Arial", size=11)
        self.pdf.cell(200, 15, txt="Temperature", align="L", ln=1)
        self.pdf.set_font("Arial", size=10)


        if float(myMin["Temperature"][0]) <= 20.0 and float(myMax["Temperature"][0]) >= 27:
            self.pdf.cell(40, 10, txt="In the room/s, where senzors "+str(myMax["Temperature"][0])+" and "+str(myMin["Temperature"][0])+" was placed, we detected both low and high temperatures.", align="L", ln=1)
        
        elif float(myMin["Temperature"][0]) <= 20.0:
            self.pdf.cell(40, 10, txt="In the room, where sensor "+str(myMin["Temperature"][0])+" was placed, we detected low temperatures.", align="L", ln=1)
            self.pdf.cell(40, 10, txt="This state is below the regulatory levels, and you can be fined for it.", align="L", ln=1)
            self.pdf.cell(40, 10, txt="To fix this issue, you should turn up your heating, or enable the invertor on your AC unit.", align="L", ln=1)

        elif float(myMax["Temperature"][0]) >= 27.0:
            self.pdf.cell(40, 10, txt="In the room, where sensor "+str(myMax["Temperature"][0])+" was placed, we detected too high temperatures.", align="L", ln=1)
            self.pdf.cell(40, 10, txt="This state is over the regulatory levels, and you can be fined for it.", align="L", ln=1)
            self.pdf.cell(40, 10, txt="To fix this issue, you should either obtain an AC unit or allow your employees to take more rest,", align="L", ln=1)
            self.pdf.cell(40, 10, txt="alongside with providing plenty of drinks.", align="L", ln=1)
        else:
            self.pdf.cell(40, 10, txt="In the room/s, where senzors was placed, we detected normal temperatures.", align="L", ln=1)

        self.pdf.ln(20)
        self.pdf.set_font("Arial", size=11)
        self.pdf.cell(200, 15, txt="Oxygen", align="L", ln=1)
        self.pdf.set_font("Arial", size=10)


        if float(myMax["Oxygen"][0]) >= 1000.0:
            self.pdf.cell(40, 10, txt="In the room where sensor "+str(myMax["Oxygen"][0])+" was placed, we detected too high CO2 level.", align="L", ln=1)
            self.pdf.cell(40, 10, txt="This state makes your employees more likely to get tired, and less concentrated.", align="L", ln=1)
            self.pdf.cell(40, 10, txt="To fix this issue, you can open the windows more often, or get an air ventilation system set up.", align="L", ln=1)
        else:
            self.pdf.cell(40, 10, txt="In the room/s, where senzors was placed, we detected safety levels of CO2.", align="L", ln=1)

        self.pdf.ln(20)




        #THIRD PAGE
        self.pdf.add_page()
        self.pdf.set_font("Arial", style="B", size=13)

        self.pdf.cell(40, 10, txt="RAW DATA", align="L")

        self.pdf.set_font("Arial", size=11)
        self.pdf.set_line_width(1)
        self.pdf.line(10, 26, 200, 26)
        self.pdf.ln(20)

        self.pdf.cell(40)
        self.pdf.cell(40, 10, txt="MINIMUN", align="L")
        self.pdf.cell(40, 10, txt="MAXIMUM", align="L")
        self.pdf.cell(40, 10, txt="AVERAGE", align="L")
        self.pdf.set_line_width(0.2)
        self.pdf.line(10, 50, 200, 50)
        self.pdf.ln(20)

        for myType in types:
            self.pdf.cell(40, 10, txt=myType, align="L")
            self.pdf.cell(40, 10, txt="%.2f %s" % (float(myMin[myType][0]), typesUnits[myType]), align="L")
            self.pdf.cell(40, 10, txt="%.2f %s" % (float(myMax[myType][0]), typesUnits[myType]), align="L")
            self.pdf.cell(40, 10, txt="%.2f %s" % (float(myAvg[myType]), typesUnits[myType]), align="L", ln=1)
            self.pdf.cell(40, 10, txt="SENSOR", align="L")
            self.pdf.cell(40, 10, txt=str(myMin[myType][2]), align="L")
            self.pdf.cell(40, 10, txt=str(myMax[myType][2]), align="L")
            self.pdf.cell(40, 10, txt="", align="L")
            self.pdf.ln(20)


        #TYPE PAGES
        for myType in types:
            self.pdf.add_page()
            self.pdf.set_font("Arial", size=11)
            self.pdf.cell(10, 10, txt=myType, ln=1)
            self.pdf.cell(20, 10, txt="TIME\\SENSORS", align="L")
            self.pdf.cell(20)
            for sensor in range(len(self.data["Senzor"])):
                self.pdf.cell(40, 10, txt=str(self.data["Senzor"][sensor][2]), align="L")
            self.pdf.set_line_width(1)
            self.pdf.line(10, 30, 200, 30)
            self.pdf.ln(20)

            #SORTING BY TIME
            sortedData = buble(self.data[myType], 2)
            for t in range(int(len(sortedData)/len(self.data["Senzor"]))):
                self.pdf.cell(20, 10, txt=str(t)+" h")
                self.pdf.cell(20)
                unsortedT = []
                
                for i in range(len(self.data["Senzor"])):
                    unsortedT.append(sortedData[i+t*(len(self.data["Senzor"]))])
                sortedT = buble(unsortedT, 1)
                for data in range(len(sortedT)):
                    self.pdf.cell(40, 10, txt=str(sortedT[data][0]), align="L")
                self.pdf.ln(10)
        
        self.pdf.output("tmp.pdf")

    def __del__(self):
        os.remove("tmp.pdf")



def buble(bubl: list, index: int, num: int = 0) -> list:
    for i in range(len(bubl)-1 -num):
        if bubl[i][index]>bubl[i+1][index]:
            mem_bub = bubl[i]
            bubl[i] = bubl[i+1]
            bubl[i+1] = mem_bub
            del mem_bub
    if num == len(bubl):
        return bubl
    else:
        return buble(bubl, index, num+1) 

