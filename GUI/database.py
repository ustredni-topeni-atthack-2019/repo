from sqlite3 import *

class database:
    def __init__(self, path_to_database = "db.db"):
        self.path = path_to_database
        try:
            self.database_file = open(self.path)
        except:
            self.error = "Database not found"
            raise MemoryError(self.error)
        self.database_file.close()
        self.conn = connect(self.path)
        self.conn.isolation_level = None
        self.key = self.conn.cursor()

    def execute(self, query):
        self.key.execute(query)
        return self.key.fetchall()

    def select(self, table: str, *items, condition: str = "1=1"):
        values = []
        for i in range(len(items)):
            values.append(str(items[i]))
        return self.execute("SELECT {} FROM {} WHERE {}".format(", ".join(values), table, condition))

    def add(self, table, **items):
        values_name = ", ".join(items.keys())
        values = []
        for item in items.keys():
            values.append(str(items[item]))
        values = "\'" + ("\', \'".join(values)) + "\'"
        return self.execute("INSERT INTO {}({}) VALUES({})".format(table, values_name, values))

